#ifndef COMPONENTMANAGER_H_INCLUDED
#define COMPONENTMANAGER_H_INCLUDED

#include <functional>
#include <string>

class Component;
class PositionComponent;
class VelocityComponent;
//class InputComponent;
class CollisionComponent;
class SpriteComponent;
class AnimationComponent;
class ExitComponent;
class HealthComponent;
class MinimapComponent;

union JsonValue;
struct JsonNode;

class ComponentManager
{
public:
    static ComponentManager& getInstance();

    ComponentManager();

    Component* createFromJson(JsonNode* node);

private:
    std::map<std::string, std::function<Component* (ComponentManager*, const JsonValue& value)> > m_factoryMap;

    Component* createPositionComponentFromJson(const JsonValue& value);
    Component* createLookdirComponentFromJson(const JsonValue& value);
    Component* createVelocityComponentFromJson(const JsonValue& value);
    Component* createCollisionComponentFromJson(const JsonValue& value);
    Component* createRenderComponentFromJson(const JsonValue& value);
    Component* createExitComponentFromJson(const JsonValue& value);
    Component* createHealthComponentFromJson(const JsonValue& value);
    Component* createMinimapComponentFromJson(const JsonValue& value);
};

#endif // COMPONENTMANAGER_H_INCLUDED
