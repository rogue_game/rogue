#ifndef ENTITYMANAGER_H_INCLUDED
#define ENTITYMANAGER_H_INCLUDED

#include<vector>
#include<string>

#include "gason.h"

using namespace std;

class Entity;
class PositionComponent;
class VelocityComponent;

class EntityManager{
public:
    static EntityManager& getInstance();

    EntityManager();

    int setPlayerPosition(PositionComponent* pos);

    Entity* m_player;

    Entity* createEntity(); //for dynamic "fill it yourself" entities
    Entity* createEntity(JsonValue value);
    Entity* createEntity(string profile, PositionComponent* pos = nullptr, VelocityComponent* vel = nullptr); //loaded from profile
    void removeEntity(Entity* ent);
    //void removeEntityByID(int id);

    vector<Entity*> m_entities; //all entities in the game

    void exit();

private:
    const unsigned short int MAX_ENTITY_COUNT = 65535;
    unsigned short int assignID();

    Entity* initFromProfile(string filename);
    Entity* initFromJson(JsonValue value);
};

#endif // ENTITYMANAGER_H_INCLUDED
