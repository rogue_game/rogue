#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <string>
#include <vector>

class Arena;
class Entity;
class ALLEGRO_BITMAP;

class Level{
public:
    static Level& getInstance();

private:
    //used to store a relative position of an arena in the minimap
    struct MapPos{
        double m_x, m_y;
        double m_w, m_h;
        MapPos(double x, double y, double w, double h);
    };

public:
    Arena* m_active;

     ALLEGRO_BITMAP* m_minimap;

    int m_id;
    std::string m_name;

    std::vector<Arena*> m_arenas;
    std::vector<MapPos> m_positions;

    Level();
    ~Level();

    int initFromFile(int id);

    int setActiveArena(int id);

    bool placeEntity(Entity* ent, int arena_id);
    bool removeEntity(Entity* ent, int arena_id);

    void drawArenaOnMinimap(int arenaId);
    void drawMinimap();

private:
    std::string idToFilepath(int id);

    double m_minimap_cx, m_minimap_cy;

};

#endif // LEVEL_H_INCLUDED
