#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <fstream>
#include <sstream>

#include "arena.h"
#include "types.h"
#include "entity.h"
#include "component.h"
#include "gason.h"
#include "level.h"
#include "tools.h"
#include "resources.h"


Entity::Entity()
{
    try
    {
        //m_id = assign_id();
    }
    catch(const char ex)
    {
        fprintf(stderr, "Failed to assign id: %s", &ex); // ?
    }

    for(int i=0; i<(int)(ComponentType::COMP_COUNT); i++)
    {
        m_compMap[ComponentType(i)] = nullptr;
    }
}

Entity::~Entity()
{
    for(int i=0; i<(int)(ComponentType::COMP_COUNT); i++)
    {
        delete(m_compMap[ComponentType(i)]);
    }
}

Component* Entity::getComponent(ComponentType compType)
{
    try
    {
        return m_compMap.at(compType);
    }
    catch(const std::out_of_range& oor)
    {
        fprintf(stderr, "Out of range error: %s\n", oor.what());
    }

    return nullptr;
}

void Entity::setComponent(Component* component)
{
    try
    {
        if(component == nullptr)
            throw std::runtime_error("Entity::setComponent: component is NULL.");
        else{
            delete(m_compMap.at(component->m_type));
            m_compMap.at(component->m_type) = component;
        }
    }
    catch(const std::out_of_range& oor)
    {
        fprintf(stderr, "Out of range error: %s", oor.what());
    }
    catch(const std::runtime_error& runerr)
    {
        fprintf(stderr, "Runtime error: %s", runerr.what());
    }
}
