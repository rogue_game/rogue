#include <iostream>

#include "constants.h"
#include "displaymanager.h"
#include "game.h"

using std::cout;
using std::endl;

DisplayManager& DisplayManager::getInstance(){
    static DisplayManager instance;
    return instance;
}

int DisplayManager::init(int flags){
    al_set_new_display_flags(flags);
    m_disp = al_create_display(screenPrefW, screenPrefH);

    if(!m_disp)
        return -1;

    calculateTransformParameters();
    return 0;
}

void DisplayManager::deinit(){
    al_destroy_display(m_disp);
}

void DisplayManager::exit(){
    al_destroy_display(m_disp);
}

void DisplayManager::handleEvent(const ALLEGRO_EVENT& ev){
    if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
        Game::getInstance().setState(Game::EXITING);
}

void DisplayManager::registerEventSource(ALLEGRO_EVENT_QUEUE* qu){
    al_register_event_source(qu, al_get_display_event_source(al_get_current_display()));
    al_set_event_source_data(al_get_display_event_source(al_get_current_display()), EV_SRC_DISPLAY);
}

void DisplayManager::calculateTransformParameters()
{
     double w = (double)al_get_display_width(al_get_current_display());
     double h = (double)al_get_display_height(al_get_current_display());

     int scaleX = w / screenPrefW;
     int scaleY = h / screenPrefH;

     m_scale = scaleX > scaleY ? scaleX : scaleY;

     m_offsetX = (double)((w - screenPrefW * (double)(m_scale))) / 2;
     m_offsetY = (double)((h - screenPrefH * (double)(m_scale))) / 2;

     m_scaleX = (w - 2*m_offsetX) / w;
     m_scaleY = (h - 2*m_offsetY) / h;
}

void DisplayManager::useTransform(){
    al_identity_transform(&m_trans);
    al_build_transform(&m_trans, m_offsetX, m_offsetY, m_scale, m_scale, 0);
    al_use_transform(&m_trans);
}

void DisplayManager::useIdentity(){
    al_identity_transform(&m_trans);
    al_use_transform(&m_trans);
}

