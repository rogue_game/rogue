#ifndef MOUSEMANAGER_H_INCLUDED
#define MOUSEMANAGER_H_INCLUDED

#include <allegro5/allegro.h>

class MouseManager{
public:
    static MouseManager& getInstance();

    double m_x;
    double m_y;

    int init();
    void handleEvent(const ALLEGRO_EVENT& ev);
    void exit();

    void registerEventSource(ALLEGRO_EVENT_QUEUE* qu);
};


#endif // MOUSEMANAGER_H_INCLUDED
