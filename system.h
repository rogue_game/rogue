#pragma once

#include<allegro5/allegro.h>

class PositionComponent;
class CollisionComponent;
class Entity;

struct MovementSystem
{
    public:
        static void tick(double dt);

};

struct InputSystem
{
    static void tick(ALLEGRO_KEYBOARD_STATE* keyboard_state);
};

struct RenderSystem
{
    static void tick();
};

struct AnimationSystem
{
    static void tick(double dt);
};

struct CollisionSystem
{
    static void tick(double dt);
};

struct HealthSystem
{
    static void tick(double dt);
};
