CC=g++
EXEC=game
OBJECTS=$(patsubst %.cpp,%.o,$(wildcard *.cpp))


ifeq ($(OS),Windows_NT)
	LDFLAGS=-I"C:\mingw\mingw32\include" -L"C:\mingw\mingw32\lib" -lallegro-5.0.10-md -lallegro_image-5.0.10-md -lallegro_primitives-5.0.10-md -lallegro_font-5.0.10-md -lallegro_ttf-5.0.10-md -llua5.1 -D DEBUG
	STDFLAG=-std=c++11
else
	LDFLAGS=-I/usr/local/include -L/usr/local/lib -lallegro -lallegro_main -lallegro_image -lallegro_primitives -lallegro_font -lallegro_ttf -llua5.1 -D DEBUG
	STDFLAG=-std=c++11
endif

all: $(EXEC)

$(EXEC): $(OBJECTS)
	$(CC) $(CXXFLAGS) $(STDFLAG) -o build/$(EXEC) $(OBJECTS) $(LDFLAGS)

arena.o: arena.cpp arena.h component.h entity.h entitymanager.h gason.h tools.h types.h resources.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c arena.cpp $(LDFLAGS)

component.o: component.cpp component.h game.h gason.h mousemanager.h resources.h system.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c component.cpp $(LDFLAGS)

componentmanager.o: componentmanager.cpp component.h componentmanager.h resources.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c componentmanager.cpp $(LDFLAGS)

displaymanager.o: displaymanager.cpp constants.h displaymanager.h game.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c displaymanager.cpp $(LDFLAGS)

entity.o: entity.cpp arena.h component.h entity.h gason.h level.h resources.h tools.h types.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c entity.cpp $(LDFLAGS)

entitymanager.o: entitymanager.cpp arena.h component.h componentmanager.h entity.h entitymanager.h level.h resources.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c entitymanager.cpp $(LDFLAGS)

game.o: game.cpp arena.h component.h constants.h displaymanager.h entity.h entitymanager.h game.h keyboardmanager.h level.h resources.h system.h tools.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c game.cpp $(LDFLAGS)

gason.o: gason.cpp gason.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c gason.cpp $(LDFLAGS)

keyboardmanager.o: keyboardmanager.cpp constants.h game.h keyboardmanager.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c keyboardmanager.cpp $(LDFLAGS)

level.o: level.cpp arena.h component.h displaymanager.h entity.h entitymanager.h game.h gason.h level.h resources.h tools.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c level.cpp $(LDFLAGS)

luascript.o: luascript.cpp luascript.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c luascript.cpp $(LDFLAGS)

main.o: main.cpp game.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c main.cpp $(LDFLAGS)

mousemanager.o: mousemanager.cpp displaymanager.h mousemanager.h 
	$(CC) $(CXXFLAGS) $(STDFLAG) -c mousemanager.cpp $(LDFLAGS)

resources.o: resources.cpp level.h resources.h tools.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c resources.cpp $(LDFLAGS)

system.o: system.cpp arena.h component.h constants.h displaymanager.h entity.h entitymanager.h game.h level.h resources.h system.h tools.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c system.cpp $(LDFLAGS)

tools.o: tools.cpp component.h entity.h tools.h
	$(CC) $(CXXFLAGS) $(STDFLAG) -c tools.cpp $(LDFLAGS)

cleanall:
	del build\$(EXEC).exe
	del $(OBJECTS)

clean:
	rm build/$(EXEC)
	rm $(OBJECTS)
