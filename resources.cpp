#include <dirent.h>
#include <sys/stat.h>
#include <cstdio>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "resources.h"
#include "tools.h"


std::map<std::string, ALLEGRO_BITMAP*> Resources::sprites;
std::map<std::string, JsonFile*> Resources::profiles;
std::map<std::string, JsonFile*> Resources::levels;
std::map<std::string, ALLEGRO_FONT*> Resources::fonts;

int Resources::load()
{
    std::string filepath;

    //SPRITES
    DIR* dp;
    struct dirent* dirp;
    struct stat filestat;

    dp = opendir("data/art");
    if(dp == nullptr)
    {
        fprintf(stderr, "Error loading sprites: %i", errno);
        return -1;
    }

    while((dirp = readdir(dp)))
    {
        filepath = "data/art/" + std::string(dirp->d_name);
        //skipping invalid directories
        if (stat(filepath.c_str(), &filestat)) continue;
        if (S_ISDIR(filestat.st_mode))         continue;

        ALLEGRO_BITMAP* sprite = al_load_bitmap(filepath.c_str());

        if(!sprite){
            fprintf(stderr, "al_load_bitmap: Error loading %s.\n", filepath.c_str());
            closedir(dp);
            return -1;
        }

        al_convert_mask_to_alpha(sprite, al_map_rgb(255,0,255));

        sprites[std::string(dirp->d_name)] = sprite;
    }

    closedir(dp);

    //PROFILES
    dp = opendir("data/profiles");
    if(dp == nullptr)
    {
        fprintf(stderr, "Error loading profiles: %i", errno);
        return -1;
    }

    while((dirp = readdir(dp)))
    {
        filepath = "data/profiles/" + std::string(dirp->d_name);
        //skipping invalid directories
        if (stat(filepath.c_str(), &filestat)) continue;
        if (S_ISDIR(filestat.st_mode))         continue;

        JsonFile* json_file = new JsonFile();

        unsigned int* buf = nullptr;
        unsigned int out_buf_size = 0;
        char* endptr = nullptr;

        if(!Utils::loadFile(filepath.c_str(), &buf, &out_buf_size))
        {
            fprintf(stderr, "loadFile: Error loading %s.\n", filepath.c_str());
            closedir(dp);
            delete json_file;
            return -1;
        }

        int status = jsonParse((char*)buf, &endptr, &json_file->value, json_file->alloc);
        if(status != JSON_OK)
        {
            fprintf(stderr, "jsonParse: Error loading %s: %s at %zd\n", filepath.c_str(), jsonStrError(status), endptr - (char*)buf);
            delete json_file;
            return -1;
        }

        profiles[std::string(dirp->d_name)] = json_file;

    }

    closedir(dp);

    //LEVELS
    dp = opendir("data/levels");
    if(dp == nullptr)
    {
        fprintf(stderr, "Error loading levels: %i", errno);
        return -1;
    }

    while((dirp = readdir(dp)))
    {
        filepath = "data/levels/" + std::string(dirp->d_name);
        //skipping invalid directories
        if (stat(filepath.c_str(), &filestat)) continue;
        if (S_ISDIR(filestat.st_mode))         continue;

        JsonFile* json_file = new JsonFile();

        unsigned int* buf = nullptr;
        unsigned int out_buf_size = 0;
        char* endptr = nullptr;

        if(!Utils::loadFile(filepath.c_str(), &buf, &out_buf_size))
        {
            fprintf(stderr, "loadFile: Error loading %s.\n", filepath.c_str());
            closedir(dp);
            delete json_file;
            return -1;
        }

        int status = jsonParse((char*)buf, &endptr, &json_file->value, json_file->alloc);
        if(status != JSON_OK)
        {
            fprintf(stderr, "jsonParse: Error loading %s: %s at %zd\n", filepath.c_str(), jsonStrError(status), endptr - (char*)buf);
            delete json_file;
            return -1;
        }

        levels[std::string(dirp->d_name)] = json_file;
    }

    //FONTS
    dp = opendir("data/fonts");
    if(dp == nullptr)
    {
        fprintf(stderr, "Error loading fonts: %i", errno);
        return -1;
    }
    while((dirp = readdir(dp)))
    {
        filepath = "data/fonts/" + std::string(dirp->d_name);
        //skipping invalid directories
        if (stat(filepath.c_str(), &filestat)) continue;
        if (S_ISDIR(filestat.st_mode))         continue;

        ALLEGRO_FONT *font = al_load_ttf_font(filepath.c_str(), 10, 0);

        if(font == nullptr){
            fprintf(stderr, "Error loading font %s. \n", filepath.c_str());
            return -1;
        }

        fonts[std::string(dirp->d_name)] = font;
    }
    return 0;
}

void Resources::free()
{
    for(auto sprite: sprites)
    {
        al_destroy_bitmap(sprite.second);
    }
    for(auto profile: profiles)
    {
        delete profile.second;
    }
    for(auto font: fonts)
    {
        al_destroy_font(font.second);
    }
}
