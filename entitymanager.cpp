#include <iostream>
#include <algorithm>
#include <stdexcept>

#include "arena.h"
#include "component.h"
#include "componentmanager.h"
#include "entity.h"
#include "entitymanager.h"
#include "level.h"
#include "resources.h"

EntityManager& EntityManager::getInstance()
{
    static EntityManager instance;
    return instance;
}
EntityManager::EntityManager()
{
    m_player = nullptr;
}

int EntityManager::setPlayerPosition(PositionComponent* pos)
{
    if(pos->m_arena_id != Level::getInstance().m_active->m_id)//Achtung! We're changing arenas!
    {
        Level::getInstance().m_active->removeEntity(m_player); //remove player from arena

        Level::getInstance().drawArenaOnMinimap(Level::getInstance().m_active->m_id); //remove the player symbol from minimap

        if(!Level::getInstance().setActiveArena(pos->m_arena_id)) //if arena successfully changed
        {
            m_player->setComponent(pos);
        }
        else
        {
            printf("Failed to change arena!\n");
            return -1;
        }

        Level::getInstance().m_active->placeEntity(m_player); //add player to the new arena (or back to the old one if the new one failed to load)
    }
    else
    {
        m_player->setComponent(pos);
    }

    return 0;
}

Entity* EntityManager::createEntity()
{
    Entity* entity = new Entity();

    try{
        entity->m_id = assignID();
    }catch(std::runtime_error &err){
        std::cout << "EntityManager::createEntity exception: " << err.what() << endl;
        delete entity;
        return nullptr;
    }

    m_entities.push_back(entity);

    return entity;
}

Entity* EntityManager::createEntity(JsonValue value)
{
    Entity* entity = initFromJson(value);

    if(entity == nullptr) //failed to initialise
        return nullptr;

    try{
        entity->m_id = assignID();
    }catch(std::runtime_error &err){
        std::cout << "EntityManager::createEntity exception: " << err.what() << endl;
        delete entity;
        entity = nullptr;
        return nullptr;
    }

    m_entities.push_back(entity);

    auto pos = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));

    if(pos != nullptr)
    {
        Level::getInstance().placeEntity(entity, pos->m_arena_id);
    }

    return entity;
}

Entity* EntityManager::createEntity(string profile, PositionComponent* pos /* = nullptr */, VelocityComponent* vel /* = nullptr */)
{

    Entity* entity = initFromProfile(profile);

    if(entity == nullptr) //failed to initialise
        return nullptr;

    if(pos != nullptr)
        entity->setComponent(pos);

    if(vel != nullptr)
        entity->setComponent(vel);

    try{
        entity->m_id = assignID();
    }catch(std::runtime_error &err){
        std::cout << "EntityManager::createEntity exception: " << err.what() << endl;
        delete entity;
        return nullptr;
    }

    m_entities.push_back(entity);

    if(pos != nullptr)
    {
        Level::getInstance().placeEntity(entity, pos->m_arena_id);
    }

    return entity;
}

Entity* EntityManager::initFromProfile(std::string filename)
{
    auto iter = Resources::profiles.find(filename);

    if(iter == Resources::profiles.end()){
        std::cout << "EntityManager::initFromProfile: Error loading profile " << filename << ". No such profile exists.\n";
        return nullptr;
    }

    JsonFile *jsonFile = Resources::profiles[filename];

    return initFromJson(jsonFile->value);
}

Entity* EntityManager::initFromJson(JsonValue value)
{
    Entity* ent = new Entity();

    for (auto i : value) {
        if(i->value.getTag() == JSON_STRING)
        {
            if(!strcmp(i->key, "profile")){
                ent = initFromProfile(i->value.toString());
            }
        }
        else if(i->value.getTag() == JSON_OBJECT)
        {
            Component* comp = ComponentManager::getInstance().createFromJson(i);
            if(comp != nullptr)
                ent->setComponent(comp);
        }
        else
        {
            printf("You've got rubbish in your .json, you filthy animal, you!");
        }
    }

    return ent;
}

void EntityManager::removeEntity(Entity* ent)
{
    auto pos = dynamic_cast<PositionComponent*>(ent->getComponent(ComponentType::POSITION));

    //entity is on an arena, possibly within current level, and needs to be removed
    if(pos != nullptr)
        Level::getInstance().removeEntity(ent, pos->m_arena_id);

    auto it = find_if(m_entities.begin(), m_entities.end(), [ent](const Entity* e){return e == ent;});

    if(it != m_entities.end())
    {
        delete (*it);
        m_entities.erase(it);
    }
}

void EntityManager::exit()
{
    for(unsigned int i = 0; i < m_entities.size(); i++)
    {
        delete m_entities[i];
    }

    m_entities.clear();
}

unsigned short int EntityManager::assignID()
{
    //this is a temporary solution...
    static unsigned short nextID = 0;
    if(nextID < MAX_ENTITY_COUNT){
        nextID++;
        return nextID;
    }
    else{
        throw std::runtime_error("EntityManager: Max entity count reached.");
        return 0;
    }
}
