#pragma once
#include <allegro5/allegro.h>

class Game
{
    public:
        enum gameState
        {
            UNINITIALISED,
            INITIALISED,
            RUNNING,
            PAUSED,
            INMENU,
            EXITING
        };

        static Game& getInstance();
        int start();
        int init();
        int exit();
        void setState(gameState newState);
        gameState getState();
        void logic(double dt);
        void render();

    private:
        Game();
        gameState m_state;
        ALLEGRO_EVENT_QUEUE* m_event_queue;
        ALLEGRO_KEYBOARD_STATE m_keyboard_state;
};
