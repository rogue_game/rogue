#include <lua5.1/lua.hpp>
#include <iostream>

#include "luascript.h"

LuaScript::LuaScript(){
    m_luaState = luaL_newstate();
    luaL_openlibs(m_luaState);

    registerFunctions();
}

LuaScript::~LuaScript(){
    lua_close(m_luaState);
}

void LuaScript::registerFunctions()
{
    /*General functions go here*/
}

void LuaScript::logError()
{
    std::cout << "Lua error: " << lua_tostring(m_luaState, -1) << ".\n";
    lua_pop(m_luaState, 1);
}

double LuaScript::readGlobalNumber(string var, const double def){
    lua_getglobal(m_luaState, var.c_str());

    if(!lua_isnumber(m_luaState, -1))
        return def;

    double result = lua_tonumber(m_luaState, -1);
    lua_pop(m_luaState, 1);
    return result;
}

string LuaScript::readGlobalString(string var, const string& def){
    lua_getglobal(m_luaState, var.c_str());

    if(!lua_isstring(m_luaState, -1))
        return def;

    int len = (int)lua_strlen(m_luaState, -1);
    std::string result(lua_tostring(m_luaState, -1), len);
    lua_pop(m_luaState, 1);

    return result;
}

string LuaScript::readGlobalStringField(string var, const int key, const string& def){
    lua_getglobal(m_luaState, var.c_str());
    lua_pushnumber(m_luaState, key);
    lua_gettable(m_luaState, -2);  /* get table[key] */

    if(!lua_isstring(m_luaState, -1))
        return def;

    string result = lua_tostring(m_luaState, -1);
    lua_pop(m_luaState, 2);  /* remove number and key*/
    return result;
}

double LuaScript::readGlobalNumberField(string var, const int key, const double def){
    lua_getglobal(m_luaState, var.c_str());
    lua_pushnumber(m_luaState, key);
    lua_gettable(m_luaState, -2); /* get table[key] */

    if(!lua_isnumber(m_luaState, -1))
        return def;

    double result = lua_tonumber(m_luaState, -1);
    lua_pop(m_luaState, 2); /* remove number and key */
    return result;
}

int LuaScript::loadScript(const char* filename){
    int res = luaL_loadfile(m_luaState, filename);

    if(res){
        logError();
    }

    luaL_dofile(m_luaState, filename);

    return 0;
}

