#ifndef ARENA_H_INCLUDED
#define ARENA_H_INCLUDED

#include <vector>
#include <string>

class Entity;
class PositionComponent;

struct ALLEGRO_BITMAP;

class Arena{
    public:
        std::string m_name;
        int m_id;
        ALLEGRO_BITMAP* m_bg;

        std::vector<Entity*> m_entities;

        Arena();
        ~Arena();

        void placeEntity(Entity* ent);
        void removeEntity(Entity* ent);


        int initFromFile(int id);
    public:

        std::string idToFilepath(int id);
};

#endif // ARENA_H_INCLUDED
