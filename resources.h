#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <map>
#include <string>

#include "gason.h"

struct JsonFile
{
    JsonValue value;
    JsonAllocator alloc;
};

class Resources
{
    public:
        static std::map<std::string, ALLEGRO_BITMAP*> sprites;
        static std::map<std::string, JsonFile*> profiles;
        static std::map<std::string, JsonFile*> levels;
        static std::map<std::string, ALLEGRO_FONT*> fonts;

        static int load();
        static void free();
};
