#include <iostream>

#include "constants.h"
#include "game.h"
#include "keyboardmanager.h"

KeyboardManager& KeyboardManager::getInstance(){
    static KeyboardManager instance;

    return instance;
}

int KeyboardManager::init(){
    if(!al_install_keyboard())
        return 0;

    return -1;
}

void KeyboardManager::handleEvent(const ALLEGRO_EVENT& ev){
    if(ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        switch(ev.keyboard.keycode)
        {
            case ALLEGRO_KEY_ESCAPE:
                Game::getInstance().setState(Game::EXITING);
            break;
        }
    }
}

void KeyboardManager::exit(){
}

void KeyboardManager::registerEventSource(ALLEGRO_EVENT_QUEUE* qu){
    al_register_event_source(qu, al_get_keyboard_event_source());
    al_set_event_source_data(al_get_keyboard_event_source(), (int)EV_SRC_KEYBOARD);
}
