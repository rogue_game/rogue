#pragma once

#include <map>

#include "constants.h"

class Component;

typedef std::map<ComponentType, Component*> compMap;
