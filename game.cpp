#include <iostream>
#include <cstdio>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#ifdef DEBUG
#include <allegro5/allegro_primitives.h>
#endif // DEBUG

#include "arena.h"
#include "component.h"
#include "constants.h"
#include "displaymanager.h"
#include "entity.h"
#include "entitymanager.h"
#include "game.h"
#include "keyboardmanager.h"
#include "level.h"
#include "mousemanager.h"
#include "resources.h"
#include "system.h"
#include "tools.h"

Game& Game::getInstance()
{
    static Game instance;
    return instance;
}
Game::Game()
{
    setState(gameState::UNINITIALISED);
}

int Game::start()
{
    setState(gameState::RUNNING);
    ALLEGRO_EVENT ev;

    double t = 0;
    double current_time = al_get_time();
    double new_time, frame_time, dt;

    while(m_state != EXITING)
    {
        new_time = al_get_time();
        frame_time = new_time - current_time;
        current_time = new_time;

        while(al_get_next_event(m_event_queue, &ev)) //handle event
        {
            int ev_src = al_get_event_source_data(ev.any.source);

            switch(ev_src)
            {
                case EV_SRC_DISPLAY:
                    DisplayManager::getInstance().handleEvent(ev);
                break;
                case EV_SRC_MOUSE:
                    MouseManager::getInstance().handleEvent(ev);
                break;
                case EV_SRC_KEYBOARD:
                    KeyboardManager::getInstance().handleEvent(ev);
                break;
            }
        }

        while(frame_time > 0.0)
        {
            dt = (frame_time < FPS)?frame_time:FPS;
            logic(dt);
            frame_time -= dt;
            t += dt;
        }
        render();

    }

    return exit();
}

int Game::init()
{
    m_event_queue = NULL;
    srand(time(NULL));
    if(!al_init())
    {
        fprintf(stderr, "Failed to initialize allegro!\n");
        return -1;
    }

    if(DisplayManager::getInstance().init(ALLEGRO_FULLSCREEN_WINDOW) == -1){
    //if(DisplayManager::getInstance().init(ALLEGRO_WINDOWED) == -1){
        fprintf(stderr, "Failed to create display!\n");
        return -1;
    }

    m_event_queue = al_create_event_queue();

#ifndef DEBUG
    al_hide_mouse_cursor(DisplayManager::getInstance().m_disp);
#endif // ndef DEBUG

    if(!m_event_queue)
    {
        fprintf(stderr, "Failed to create event_queue!\n");
        DisplayManager::getInstance().deinit();
        return -1;
    }

    if(!al_init_image_addon())
    {
        fprintf(stderr, "Failed to initialise image addon!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
        return -1;
    }

    if(!al_init_ttf_addon()){
        fprintf(stderr, "Failed to load font addon!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
        return -1;
    }

#ifdef DEBUG
    if(!al_init_primitives_addon())
    {
        fprintf(stderr, "Failed to initialise primitives addon!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
    }
#endif // DEBUG

    if(Resources::load() == -1){
        fprintf(stderr, "Failed to load resources!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
        Resources::free(); //if any resources were loaded, free them
        return -1;
    }

    if(!MouseManager::getInstance().init()){
        fprintf(stderr, "Failed to register mouse driver!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
        Resources::free();
        return -1;
    }

    if(!KeyboardManager::getInstance().init()){
        fprintf(stderr, "Failed to register keyboard driver!\n");
        DisplayManager::getInstance().deinit();
        al_destroy_event_queue(m_event_queue);
        Resources::free();
        return -1;
    }

    DisplayManager::getInstance().registerEventSource(m_event_queue);
    MouseManager::getInstance().registerEventSource(m_event_queue);
    KeyboardManager::getInstance().registerEventSource(m_event_queue);

    Level::getInstance().initFromFile(0);
    if(Level::getInstance().setActiveArena(0) == -1){
        std::cout<< "Arena not found.\n";
    }

    EntityManager::getInstance().m_player = EntityManager::getInstance().createEntity("player.json", new PositionComponent(96, 96, 0));
    EntityManager::getInstance().m_player->setComponent(new InputComponent());

    HealthComponent::bar = Resources::sprites["healthbar.bmp"];
    setState(gameState::INITIALISED);

    return 0;
}

int Game::exit()
{
    setState(gameState::EXITING);

    EntityManager::getInstance().exit();

    Resources::free();

    al_destroy_event_queue(m_event_queue);

    DisplayManager::getInstance().exit();

    return 0;
}
void Game::logic(double dt)
{
    InputSystem::tick(&m_keyboard_state);
    MovementSystem::tick(dt);
    CollisionSystem::tick(dt);
    AnimationSystem::tick(dt);
    HealthSystem::tick(dt);
}

void Game::render()
{
    al_clear_to_color(al_map_rgb(0,0,0));

    //transformed drawing
    DisplayManager::getInstance().useTransform();

    RenderSystem::tick();
#if DEBUG
    al_draw_textf(Resources::fonts["times.ttf"], al_map_rgb(255,255,255), 0, 0, 0, "Mouse: %f / %f", MouseManager::getInstance().m_x, MouseManager::getInstance().m_y);
#endif //DEBUG

    DisplayManager::getInstance().useIdentity();

    //untransformed drawing (gui, text?)
    al_draw_bitmap(Resources::sprites["cursor.bmp"], MouseManager::getInstance().m_x, MouseManager::getInstance().m_y, 0);

    al_flip_display();
}

void Game::setState(gameState newState)
{
    m_state = newState;
}

Game::gameState Game::getState()
{
    return m_state;
}
