#include<cstdlib>

#include<sstream>
#include<string>
#include<cstdio>
#include<cmath>

#include "component.h"
#include "entity.h"
#include "tools.h"

bool Utils::loadFile( const char* filename, unsigned int** out_buf_ptr, unsigned int* out_buf_size)
{
    FILE* file = fopen(filename,"rt");

    if (file == nullptr)
        return false;

    fseek(file, 0, SEEK_END);
    long pos = ftell(file);
    fseek(file, 0, SEEK_SET);

    *out_buf_size = (unsigned int)pos;
    *out_buf_ptr = new unsigned int[pos + 1];
    const size_t real_read = fread(*out_buf_ptr, 1, *out_buf_size, file);
    fclose(file);
    (*out_buf_ptr)[real_read] = '\0';

    return true;
}

double Utils::vecLenght2d(double x, double y)
{
    return sqrt(x*x + y*y);
}
