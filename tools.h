#pragma once

namespace Utils
{
    /** Read file into string. */
    bool loadFile( const char* filename, unsigned int** out_buf_ptr, unsigned int* out_buf_size);

    double vecLenght2d(double x, double y);
};
