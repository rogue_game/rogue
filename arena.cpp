#include "arena.h"
#include "component.h"
#include "entity.h"
#include "entitymanager.h"
#include "gason.h"
#include "tools.h"
#include "types.h"
#include "resources.h"

Arena::Arena(){
    m_id = -1;
}

Arena::~Arena(){

}

void Arena::placeEntity(Entity* ent){
    m_entities.push_back(ent);
}

void Arena::removeEntity(Entity* ent){
    for(auto it = m_entities.begin(); it != m_entities.end(); it++){
        if(ent == (*it)){
            m_entities.erase(it);
            return;
        }
    }

    return;
}

int Arena::initFromFile(int id)
{
    m_id = id;
    std::string filepath = idToFilepath(id);

    unsigned int* buf = nullptr;
    unsigned int out_buf_size = 0;
    char* endptr = nullptr;

    if(!Utils::loadFile(filepath.c_str(), &buf, &out_buf_size))
    {
        fprintf(stderr, "loadFile: Error loading %s.\n", filepath.c_str());
        return -1;
    }

    JsonFile* jsonFile = new JsonFile();

    int status = jsonParse((char*)buf, &endptr, &jsonFile->value, jsonFile->alloc);
    if(status != JSON_OK)
    {
        fprintf(stderr, "jsonParse: Error loading %s: %s at %zd\n", filepath.c_str(), jsonStrError(status), endptr - (char*)buf);
        delete jsonFile;
        return -1;
    }
    for (auto i : jsonFile->value) {
        if(i->value.getTag() == JSON_STRING)
        {
            if(!strcmp(i->key, "bg")){
                m_bg = Resources::sprites[i->value.toString()];
            }
            else if(!strcmp(i->key, "name")){
                m_name = i->value.toString();
            }
        }
        else if(i->value.getTag() == JSON_OBJECT)
        {
            if(!strcmp(i->key, "entity")){
                Entity *ent = EntityManager::getInstance().createEntity(i->value);
                if(ent == nullptr){
                    printf("Failed to load entity from profile.\n");
                    delete ent;
                }
            }
        }
     }

    return 0;
}

std::string Arena::idToFilepath(int arenaID){
    std::string filepath = "data/arenas/arena";
    std::string strid = std::to_string(arenaID);

    filepath.append(4 - strid.length(), '0');

    filepath += std::to_string(arenaID)+".json";

    return filepath;
}
