#include <allegro5/allegro.h>
#include <iostream>

#include "component.h"
#include "game.h"
#include "gason.h"
#include "resources.h"

ALLEGRO_BITMAP* HealthComponent::bar;

PositionComponent::PositionComponent(double x, double y, int arena_id, direction dir) : Component(ComponentType::POSITION)
{
    m_x = x;
    m_y = y;
    m_arena_id = arena_id;
    m_direction = dir;
}

VelocityComponent::VelocityComponent(double speed, double vel_x, double vel_y) : Component(ComponentType::VELOCITY)
{
    m_speed = speed;
    m_x = vel_x;
    m_y = vel_y;
}

VelocityComponent::VelocityComponent(double speed) : Component (ComponentType::VELOCITY)
{
    m_speed = speed;
}

CollisionComponent::CollisionComponent(double w, double h, int mask) : Component(ComponentType::COLLISION)
{
    m_w = w;
    m_h = h;
    m_mask = mask;
}

InputComponent::InputComponent() : Component(ComponentType::INPUT)
{

}

RenderComponent::~RenderComponent(){
    for(auto anim: m_animations){
        delete anim;
    }
    m_animations.clear();
}

void RenderComponent::addAnimation(Animation *anim)
{
    m_animations.push_back(anim);
}

void RenderComponent::start(AnimType animType, double frameTime, int repeat)
{
    if(m_playing & animType){
        return;
    }

    for(auto animation: m_animations){
        if(!(animation->m_animTypes & animType)){
            animation->stop();
            continue;
        }

        //which animations are contained in the sprite? So that we know the x-position in the sprite of anim
        int countAnim = 0;
        for(int prevAnim = 1; prevAnim < (int)animType; prevAnim*=2)
        {
            if(animation->m_animTypes & prevAnim)
                countAnim++;
        }

        animation->m_timeStarted = al_get_time();
        animation->m_frame = 0;
        animation->m_frameTime = frameTime;
        animation->m_repeat = repeat;
        animation->m_column = countAnim;
    }

    m_playing = animType;
}

void RenderComponent::stop()
{
    m_playing = NONE;

    for(auto animation: m_animations){
        animation->stop();
    }
}

void RenderComponent::Animation::stop()
{
    m_frame = 0;
    m_column = 0;
}

ExitComponent::ExitComponent(double x, double y, int arena, direction dir) : Component(ComponentType::EXIT)
{
    m_x = x;
    m_y = y;
    m_arena = arena;
    m_direction = dir;
}

HealthComponent::HealthComponent(int maxhp, int currenthp, std::string body) : Component(ComponentType::HEALTH)
{
    m_max = maxhp;
    m_current = currenthp;

    m_body = body;
}

MinimapComponent::MinimapComponent(bool visible, ALLEGRO_BITMAP* symbol) : Component(ComponentType::MINIMAP)
{
    m_visible = visible;
    m_symbol = symbol;
}

