#ifndef LUASCRIPT_H_INCLUDED
#define LUASCRIPT_H_INCLUDED

#include <string>

using namespace std;

class lua_State;

class LuaScript{
private:
    lua_State* m_luaState;

    void registerFunctions();
    void logError();

    double readGlobalNumber(string var, const double def);
    string readGlobalString(string var, const string& def);

    double readGlobalNumberField(string var, const int key, const double def);
    string readGlobalStringField(string var, const int key, const string& def);

public:
    LuaScript();
    ~LuaScript();

    int loadScript(const char* filename);
};

#endif // LUASCRIPT_H_INCLUDED
