#ifndef KEYBOARDMANAGER_H_INCLUDED
#define KEYBOARDMANAGER_H_INCLUDED

#include <allegro5/allegro.h>

class KeyboardManager{
public:
    static KeyboardManager& getInstance();

    int init();

    void handleEvent(const ALLEGRO_EVENT &ev);

    void exit();

    void registerEventSource(ALLEGRO_EVENT_QUEUE* qu);
};

#endif // KEYBOARDMANAGER_H_INCLUDED
