#include <cmath>
#include <cstdio>
#include <iostream>

#ifdef DEBUG
#include <allegro5/allegro_primitives.h>
#endif // DEBUG

#include "arena.h"
#include "component.h"
#include "constants.h"
#include "displaymanager.h"
#include "entity.h"
#include "entitymanager.h"
#include "game.h"
#include "level.h"
#include "resources.h"
#include "system.h"
#include "tools.h"

void MovementSystem::tick(double dt)
{
    //for(auto entity: Entity::entities)
    for(auto entity: Level::getInstance().m_active->m_entities)
    {

        auto pos = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));
        auto vel = dynamic_cast<VelocityComponent*>(entity->getComponent(ComponentType::VELOCITY));
        auto col = dynamic_cast<CollisionComponent*>(entity->getComponent(ComponentType::COLLISION));

        if(pos == nullptr || vel == nullptr)
            continue;

        double old_pos_x = pos->m_x;
        double old_pos_y = pos->m_y;

        pos->m_x += (vel->m_x * vel->m_speed ) * dt;
        pos->m_y += (vel->m_y * vel->m_speed ) * dt;

        if(!col)
            continue;

        // prevent entities from leaving the screen
        if(pos->m_x - (col->m_w / 2) <= 0.0 || pos->m_x + (col->m_w / 2) >= DisplayManager::getInstance().screenPrefW)
            pos->m_x = old_pos_x;
        if(pos->m_y - (col->m_h / 2) <= 0.0 || pos->m_y + (col->m_h / 2) >= DisplayManager::getInstance().screenPrefH)
            pos->m_y = old_pos_y;

        auto look = dynamic_cast<LookdirComponent*>(entity->getComponent(ComponentType::LOOKDIR));
        if(look != nullptr){
            if(look->m_followX != nullptr && look->m_followY != nullptr){
                //mouse position is kept in window-space -- before calculating the angle need transform into world-space
                double mX = *look->m_followX;
                double mY = *look->m_followY;
                DisplayManager::getInstance().transformWindowToWorld(&mX, &mY);

                look->m_angle = PI/2 - std::atan2(pos->m_x  - mX, pos->m_y - mY)/2;
            }
        }

    }

}

void InputSystem::tick(ALLEGRO_KEYBOARD_STATE* keyboard_state)
{
    al_get_keyboard_state(keyboard_state);

    //for(auto entity: Entity::entities)
    for(auto entity: Level::getInstance().m_active->m_entities)
    {
        auto input = dynamic_cast<InputComponent*>(entity->getComponent(ComponentType::INPUT));
        auto pos   = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));
        auto vel = dynamic_cast<VelocityComponent*>(entity->getComponent(ComponentType::VELOCITY));
        auto health = dynamic_cast<HealthComponent*>(entity->getComponent(ComponentType::HEALTH));

        if(pos == nullptr || vel == nullptr || input == nullptr) //are pos and vel really necessary for input?
            continue;

        double old_speed = Utils::vecLenght2d(vel->m_x, vel->m_y);

        // there must be a way to do this more efficiently
        if(al_key_down(keyboard_state, ALLEGRO_KEY_RIGHT) && !al_key_down(keyboard_state, ALLEGRO_KEY_LEFT))
        {
            vel->m_x = 1;
            pos->m_direction = DIR_E;
        }

        else if(al_key_down(keyboard_state, ALLEGRO_KEY_LEFT) && !al_key_down(keyboard_state, ALLEGRO_KEY_RIGHT))
        {
            vel->m_x = -1;
            pos->m_direction = DIR_W;
        }

        else
            vel->m_x = 0;

        if(al_key_down(keyboard_state, ALLEGRO_KEY_UP) && !al_key_down(keyboard_state, ALLEGRO_KEY_DOWN))
        {
            vel->m_y = -1;
            pos->m_direction = DIR_N; //yup
        }

        else if(al_key_down(keyboard_state, ALLEGRO_KEY_DOWN) && !al_key_down(keyboard_state, ALLEGRO_KEY_UP))
        {
            vel->m_y = 1;
            pos->m_direction = DIR_S;
        }
        else
            vel->m_y = 0;

        if(al_key_down(keyboard_state, ALLEGRO_KEY_SPACE))
        {
            health->m_current -= health->m_max;
        }

        auto render = dynamic_cast<RenderComponent*>(entity->getComponent(ComponentType::RENDER));
        if(render){
        //animation changes
            if(old_speed == 0 && Utils::vecLenght2d(vel->m_x, vel->m_y) > 0)
            //started walking
                render->start(RenderComponent::AnimType::WALK, 10./vel->m_speed);
            else if (old_speed > 0 && Utils::vecLenght2d(vel->m_x, vel->m_y) == 0)
            //stopped walking => is idle...?
                render->start(RenderComponent::AnimType::IDLE, 1);
        }
    }
}

void RenderSystem::tick()
{
    Level::getInstance().drawMinimap();

    al_draw_bitmap(Level::getInstance().m_active->m_bg, 0, 0, 0);

    //for(auto entity: Entity::entities)
    for(auto entity: Level::getInstance().m_active->m_entities)
    {
        auto pos = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));
        auto render = dynamic_cast<RenderComponent*>(entity->getComponent(ComponentType::RENDER));

#ifdef DEBUG
        auto col  = dynamic_cast<CollisionComponent*>(entity->getComponent(ComponentType::COLLISION));

        if(col != nullptr)
        {
            auto ux = pos->m_x - col->m_w/2;
            auto uy = pos->m_y - col->m_h/2;
            auto lx = ux + col->m_w;
            auto ly = uy + col->m_h;
            al_draw_rectangle(ux, uy, lx, ly, al_map_rgb(255,0,0), 1);
        }
#endif // DEBUG

        if(pos == nullptr || render == nullptr)
            continue;

        auto look = dynamic_cast<LookdirComponent*>(entity->getComponent(ComponentType::LOOKDIR));

        if(!look){
            for(auto anim: render->m_animations){
                int row = (int)pos->m_direction;
                al_draw_bitmap_region(anim->m_sprite, anim->m_x, anim->m_y+anim->m_h*row, anim->m_w, anim->m_h, pos->m_x + anim->m_offX -anim->m_w/2, pos->m_y + anim->m_offY - anim->m_h/2, 0);
            }
            continue;
        }

        RenderComponent::Animation* anim;

        bool frontToBack = (bool)(PI/4 > look->m_angle || look->m_angle > 3*PI/4);

        unsigned int animCount = render->m_animations.size();
        for(int i=0; i < animCount; i++){
            if(frontToBack){
                anim = render->m_animations[i];
            }else{
                anim = render->m_animations[animCount-1-i];
            }

            int row = 0;
            row = ((int)(anim->m_dirs*look->m_angle / PI + 0.5)) % anim->m_dirs;

            //do we have different offsets for different directions? If yes, use them.
            int offX = anim->m_offXvec.empty() ? anim->m_offX : anim->m_offXvec.at(row);
            int offY = anim->m_offYvec.empty() ? anim->m_offY : anim->m_offYvec.at(row);

            if(anim->m_rotate){
                double rotationAngle = 2*(look->m_angle - row * PI / anim->m_dirs);

                //do we have different rotation centers for different directions? If yes, use them.
                int rotX = anim->m_rotXvec.empty() ? anim->m_rotX : anim->m_rotXvec.at(row);
                int rotY = anim->m_rotYvec.empty() ? anim->m_rotY : anim->m_rotYvec.at(row);

                //ALLEGRO_TRANSFORM + al_draw_bitmap_region may be faster... but this works for now
                ALLEGRO_BITMAP* sub = al_create_sub_bitmap(anim->m_sprite, anim->m_x, anim->m_y + anim->m_h*row, anim->m_w, anim->m_h);
                al_draw_rotated_bitmap(sub, rotX, rotY, pos->m_x + offX, pos->m_y + offY, rotationAngle, 0);
                al_destroy_bitmap(sub);
            }
            else
                al_draw_bitmap_region(anim->m_sprite, anim->m_x, anim->m_y+anim->m_h*row, anim->m_w, anim->m_h, pos->m_x + offX -anim->m_w/2, pos->m_y +  offY - anim->m_h/2, 0);
        }

        auto hel = dynamic_cast<HealthComponent*>(entity->getComponent(ComponentType::HEALTH));

        if(hel == nullptr)
            continue;
        else
        {
            //al_draw_bitmap_region(HealthComponent::bar, 0, 0, 32, 4, pos->m_x - 16, pos->m_y - anim.m_sprite->m_h/2, 0);
            //al_draw_bitmap_region(HealthComponent::bar, 0, 4, 32*hel->m_current/hel->m_max, 4, pos->m_x - 16, pos->m_y - sprite->m_h/2, 0);
        }
    }
}

void AnimationSystem::tick(double dt)
{
    //for(auto entity: Entity::entities)
    for(auto entity: Level::getInstance().m_active->m_entities)
    {
        auto render = dynamic_cast<RenderComponent*>(entity->getComponent(ComponentType::RENDER));

        if(render == nullptr)
            continue;

        //set current frame
        for(auto anim: render->m_animations){
            if(render->m_playing)
                anim->m_frame = (int)((al_get_time() - anim->m_timeStarted) / anim->m_frameTime) % render->MAX_ANIM_FRAMES;

            anim->m_x = (anim->m_column * render->MAX_ANIM_FRAMES + anim->m_frame) * anim->m_w;
        }
    }
}

void CollisionSystem::tick(double dt)
{
    for(auto entity: Level::getInstance().m_active->m_entities)
    {
        auto vel1 = dynamic_cast<VelocityComponent*>(entity->getComponent(ComponentType::VELOCITY));
        auto pos1 = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));
        auto col1 = dynamic_cast<CollisionComponent*>(entity->getComponent(ComponentType::COLLISION));

        if (!vel1)
            continue;

        if(col1 && pos1)
        {
            //for(auto col_entity: Entity::entities)
            for(auto col_entity: Level::getInstance().m_active->m_entities)
            {
                if(entity == col_entity)
                    break;

                auto pos2 = dynamic_cast<PositionComponent*>(col_entity->getComponent(ComponentType::POSITION));
                auto col2 = dynamic_cast<CollisionComponent*>(col_entity->getComponent(ComponentType::COLLISION));

                if(!pos2 || !col2)
                    continue;

                double overlap_x = abs(pos1->m_x - pos2->m_x) - (col1->m_w + col2->m_w)/2;
                double overlap_y = abs(pos1->m_y - pos2->m_y) - (col1->m_h + col2->m_h)/2;

                if(overlap_x < 0 && overlap_y < 0)
                {
                    if(col2->m_mask & CollisionComponent::CollisionFlag::BLOCK){
                        if (vel1->m_x != 0 && overlap_x > overlap_y)
                        {
                            if(pos1->m_x - col1->m_w <= pos2->m_x + col2->m_w/2 && pos2->m_x + col2->m_w/2 <= pos1->m_x + col1->m_w)
                            {
                                if(pos1->m_x - col1->m_w <= pos2->m_x - col2->m_w/2 && pos2->m_x - col2->m_w/2 <= pos1->m_x + col1->m_w)
                                {
                                    vel1->m_x > 0 ? pos1->m_x += overlap_x : pos1->m_x -= overlap_x;
                                }
                                else
                                    pos1->m_x -= overlap_x;
                            }
                            else
                            {
                                if(pos1->m_x - col1->m_w <= pos2->m_x - col2->m_w/2 && pos2->m_x - col2->m_w/2 <= pos1->m_x + col1->m_w)
                                {
                                    pos1->m_x += overlap_x;
                                }
                            }
                        }
                        if (vel1->m_y != 0 && overlap_y > overlap_x)
                        {
                            if(pos1->m_y - col1->m_h <= pos2->m_y + col2->m_h/2 && pos2->m_y + col2->m_h/2 <= pos1->m_y + col1->m_h)
                            {
                                if(pos1->m_y - col1->m_h <= pos2->m_y - col2->m_h/2 && pos2->m_y - col2->m_h/2 <= pos1->m_y + col1->m_h)
                                {
                                    vel1->m_y > 0 ? pos1->m_y += overlap_y : pos1->m_y -= overlap_y;
                                }
                                else
                                    pos1->m_y -= overlap_y;
                            }
                            else
                            {
                                if(pos1->m_y - col1->m_h <= pos2->m_y - col2->m_h/2 && pos2->m_y - col2->m_h/2 <= pos1->m_y + col1->m_h)
                                {
                                    pos1->m_y += overlap_y;
                                }
                            }
                        }
                    }

                    if((col1->m_mask & CollisionComponent::CollisionFlag::PLAYER) && (col2->m_mask & CollisionComponent::CollisionFlag::EXIT)){
                        auto ext = dynamic_cast<ExitComponent*>(col_entity->getComponent(ComponentType::EXIT));
                        if(ext){
                            EntityManager::getInstance().setPlayerPosition(new PositionComponent(ext->m_x, ext->m_y, ext->m_arena, ext->m_direction));
                        }
                    }
                }
            }
        }
    }
}

void HealthSystem::tick(double dt)
{
    for(auto entity: Level::getInstance().m_active->m_entities)
    {
        auto hel = dynamic_cast<HealthComponent*>(entity->getComponent(ComponentType::HEALTH));

        if(!hel)
            continue;

        if(hel->m_current <= 0)
        {
            auto pos = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));

            if(pos)
                EntityManager::getInstance().createEntity(hel->m_body, new PositionComponent(pos->m_x, pos->m_y, pos->m_arena_id));

            EntityManager::getInstance().removeEntity(entity);
        }
    }
}

