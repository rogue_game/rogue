#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <map>

const double PI = 3.1415926535897932385;

const float FPS = 1./60;

const int DISPLAY_W = 840;
const int DISPLAY_H = 480;

enum event_source
{
    EV_SRC_DISPLAY = 0,
    EV_SRC_MOUSE = 1,
    EV_SRC_KEYBOARD = 2
};

enum direction
{
    DIR_S = 0,
    DIR_SW,
    DIR_W,
    DIR_NW,
    DIR_N,
    DIR_NE,
    DIR_E,
    DIR_SE,
};

enum struct ComponentType
{
    POSITION,
    LOOKDIR,
    VELOCITY,
    COLLISION,
    INPUT,
    RENDER,
    AI,
    EXIT,
    HEALTH,
    MINIMAP,
    COMP_COUNT
};

#endif // CONSTANTS_H
