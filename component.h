#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED

#include <string>
#include <vector>

#include "constants.h"
#include "entity.h"
#include "gason.h"

class ALLEGRO_BITMAP;

class Component
{
public:
    virtual ~Component(){}

    Component(ComponentType type) : m_type(type){}

    ComponentType m_type;
};

class PositionComponent : public Component
{
    public:
        PositionComponent(double x, double y, int arena_id = 0, direction dir = DIR_S);
        PositionComponent() : m_x(0), m_y(0), m_direction(DIR_S), m_arena_id(0), Component(ComponentType::POSITION){}

        ~PositionComponent(){}

        double m_x, m_y;
        int m_arena_id;
        direction m_direction;
};

class LookdirComponent : public Component
{
public:
    LookdirComponent(double angle = 0) : m_angle(angle), m_followX(nullptr), m_followY(nullptr), Component(ComponentType::LOOKDIR){}

    double m_angle;
    double *m_followX, *m_followY;
};

class VelocityComponent : public Component
{
    public:
        VelocityComponent(double speed);
        VelocityComponent(double speed, double vel_x, double vel_y);
        VelocityComponent() : m_speed(0), m_x(0), m_y(0), Component(ComponentType::VELOCITY){}

        ~VelocityComponent(){}

        double m_speed;
        double m_x;
        double m_y;
};

class InputComponent : public Component
{
    public:
        InputComponent();

        ~InputComponent(){}
};

class CollisionComponent : public Component
{
public:
    enum CollisionFlag{
        BLOCK   = 0x1,
        PLAYER  = 0x2,
        EXIT    = 0x4,
        MONSTER = 0x8,
        PROJ    = 0x16,
        TRAP    = 0x32,
        CUSTOM  = 0x64
    };

    public:
        CollisionComponent(double m_w, double m_h, int mask = CollisionFlag::BLOCK);
        CollisionComponent() : m_w(0), m_h(0), m_mask(CollisionFlag::BLOCK), Component(ComponentType::COLLISION){}

        ~CollisionComponent(){}

        double m_w, m_h;

        int m_mask;

    friend class CollisionSystem;
    friend class Entity;
};

class RenderComponent : public Component
{
public:
    enum AnimType{
        NONE   = 0,
        IDLE   = 1,
        WALK   = 2,
        TALK   = 4,
        MELEE  = 8,
        RANGED = 16,
        ANIM_COUNT = 32
    };

    struct Animation{
        Animation(): m_sprite(nullptr), m_w(0), m_h(0), m_frameTime(1), m_dirs(1), m_rotate(false), m_animTypes(NONE), m_offX(0), m_offY(0), m_rotX(0), m_rotY(0),
        m_x(0), m_y(0), m_angle(0), m_frame(0), m_repeat(0), m_timeStarted(0) {};


        Animation(ALLEGRO_BITMAP* sprite, std::string tag, int w, int h, double frameTime, int dirs, bool rotate, int animTypes, int offX = 0, int offY = 0, int rotX = 0, int rotY = 0)
        : m_tag(tag), m_sprite(sprite), m_w(w), m_h(h), m_frameTime(frameTime), m_dirs(dirs), m_rotate(rotate), m_animTypes(animTypes), m_offX(offX), m_offY(offY), m_rotX(rotX), m_rotY(rotY),
        m_x(0), m_y(0), m_angle(0), m_frame(0), m_repeat(0), m_timeStarted(0) {};

        ALLEGRO_BITMAP* m_sprite;
        std::string m_tag;

        int m_dirs;
        bool m_rotate; //does the sprite rotate?
        int m_rotX, m_rotY; //what is the rotation centre?
        std::vector<int> m_rotXvec;
        std::vector<int> m_rotYvec;
        int m_animTypes;

        int m_offX, m_offY; //offset wr to the position
        std::vector<int> m_offXvec; //different offsets in diffferent directions?
        std::vector<int> m_offYvec;

        int m_x, m_y; //current position in the sprite (direction/frame)
        double m_column;
        double m_angle; //for rotations, only used in ANIM_CNTNS

        int m_w, m_h; //size of a frame

        int m_frame;
        int m_repeat;

        double m_frameTime;
        double m_timeStarted;

        void stop();
    };

    RenderComponent() : m_playing(NONE), Component(ComponentType::RENDER){}
    ~RenderComponent();

    AnimType m_playing;
    std::vector<Animation*> m_animations;

    void addAnimation(Animation *animation);
    void start(AnimType anim, double frameTime, int repeat = 0);

    Animation* getAnimationByTag(const std::string &tag) const;

    const int MAX_ANIM_FRAMES = 4;

private:
    void stop();
};

class ExitComponent : public Component
{
    public:
        ExitComponent(double x, double y, int arena, direction dir = DIR_S);
        ExitComponent() : m_x(0), m_y(0), m_arena(-1), m_direction(DIR_S), Component(ComponentType::EXIT){}

        ~ExitComponent(){}

        double m_x;
        double m_y;
        int m_arena;
        direction m_direction;
};

class HealthComponent : public Component
{
    public:
        static ALLEGRO_BITMAP* bar;

        HealthComponent() : m_max(1), m_current(1), Component(ComponentType::HEALTH){}
        HealthComponent(int maxhp, int currenthp, std::string boy = "dummy");

        ~HealthComponent(){}

        int m_max;
        int m_current;

        std::string m_body; //profile of a body
};

class MinimapComponent : public Component
{
public:
    MinimapComponent() : m_visible(false), m_symbol(nullptr), Component(ComponentType::MINIMAP){}
    MinimapComponent(bool visible, ALLEGRO_BITMAP* symbol);

    ~MinimapComponent(){}

    bool m_visible;
    ALLEGRO_BITMAP* m_symbol;
};
#endif
