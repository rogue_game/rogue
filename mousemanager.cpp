#include<iostream>

#include "constants.h"
#include "displaymanager.h"
#include "mousemanager.h"

MouseManager& MouseManager::getInstance()
{
    static MouseManager instance;

    return instance;
}

int MouseManager::init(){
    if(!al_install_mouse())
        return 0;

    return -1;
}

void MouseManager::handleEvent(const ALLEGRO_EVENT& ev){
    m_x = ev.mouse.x;
    m_y = ev.mouse.y;

    DisplayManager::getInstance().transformScreenToWindow(&m_x, &m_y);

    //transformed stuff -- mechanics
}

void MouseManager::exit(){

}

void MouseManager::registerEventSource(ALLEGRO_EVENT_QUEUE* qu){
    al_register_event_source(qu, al_get_mouse_event_source());
    al_set_event_source_data(al_get_mouse_event_source(), (int)EV_SRC_MOUSE);
}

