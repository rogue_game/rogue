#include <cstring>
#include <iostream>

#include "component.h"
#include "componentmanager.h"
#include "mousemanager.h"
#include "resources.h"
#include "system.h"

ComponentManager& ComponentManager::getInstance(){
    static ComponentManager instance;

    return instance;
}

ComponentManager::ComponentManager(){
    m_factoryMap["position"] = &ComponentManager::createPositionComponentFromJson;
    m_factoryMap["lookdir"] = &ComponentManager::createLookdirComponentFromJson;
    m_factoryMap["velocity"] = &ComponentManager::createVelocityComponentFromJson;
    m_factoryMap["collision"] = &ComponentManager::createCollisionComponentFromJson;
    m_factoryMap["render"] = &ComponentManager::createRenderComponentFromJson;
    m_factoryMap["exit"] = &ComponentManager::createExitComponentFromJson;
    m_factoryMap["health"] = &ComponentManager::createHealthComponentFromJson;
    m_factoryMap["minimap"] = &ComponentManager::createMinimapComponentFromJson;
}

Component* ComponentManager::createFromJson(JsonNode* node)
{
    auto factory = m_factoryMap.find(std::string(node->key));

    if(factory == m_factoryMap.end()){
        fprintf(stderr, "ComponentManager::createFromJson: invalid JsonNode key %s\n", node->key);
        return nullptr;
    }

    return (factory->second)(this, node->value);
}

Component* ComponentManager::createPositionComponentFromJson(const JsonValue& value){
    PositionComponent* comp = new PositionComponent();

    for(auto data: value)
    {
        if(!strcmp(data->key, "x"))
            comp->m_x = data->value.toNumber();
        else if(!strcmp(data->key, "y"))
            comp->m_y = data->value.toNumber();
        else if(!strcmp(data->key, "arena_id"))
            comp->m_arena_id = data->value.toNumber();
        else if(!strcmp(data->key, "direction"))
            comp->m_direction = static_cast<direction>(data->value.toNumber());
    }

    return comp;
}

Component* ComponentManager::createLookdirComponentFromJson(const JsonValue& value){
    LookdirComponent* comp = new LookdirComponent();

    for(auto data: value)
    {
        if(!strcmp(data->key, "follow")){
            std::string follow = data->value.toString();
            if(follow == "cursor"){
                comp->m_followX = &MouseManager::getInstance().m_x;
                comp->m_followY = &MouseManager::getInstance().m_y;
            }
        }
    }

    return comp;
}

Component* ComponentManager::createVelocityComponentFromJson(const JsonValue& value){
    VelocityComponent* comp = new VelocityComponent();

    for(auto data: value)
    {
        if(!strcmp(data->key, "speed")){
            comp->m_speed = data->value.toNumber();
        }
    }

    return comp;
}

Component* ComponentManager::createCollisionComponentFromJson(const JsonValue& value){
    CollisionComponent* comp = new CollisionComponent();

    for(auto data: value){
        if(!strcmp(data->key, "width"))
            comp->m_w = data->value.toNumber();
        if(!strcmp(data->key, "height"))
            comp->m_h = data->value.toNumber();
        if(!strcmp(data->key, "mask"))
            comp->m_mask = data->value.toNumber();
    }

    return comp;
}

Component* ComponentManager::createRenderComponentFromJson(const JsonValue& value){
    RenderComponent* render = new RenderComponent();

    for(auto data: value)
    {
        if(!strcmp(data->key, "animation")){
            RenderComponent::Animation *anim = new RenderComponent::Animation();

            for(auto animdata: data->value){
                if(!strcmp(animdata->key, "sprite"))
                    anim->m_sprite = Resources::sprites[animdata->value.toString()];
                else if(!strcmp(animdata->key, "tag"))
                    anim->m_tag = animdata->value.toString();
                else if(!strcmp(animdata->key, "w"))
                    anim->m_w = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "h"))
                    anim->m_h = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "offX"))
                    anim->m_offX = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "offY"))
                    anim->m_offY = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "rotate"))
                    anim->m_rotate = (bool)animdata->value.toNumber();
                else if(!strcmp(animdata->key, "rotX"))
                    anim->m_rotX = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "rotY"))
                    anim->m_rotY = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "frame_time"))
                    anim->m_frameTime = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "dirs"))
                    anim->m_dirs = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "types"))
                    anim->m_animTypes = animdata->value.toNumber();
                else if(!strcmp(animdata->key, "rotXvec")){
                    for(auto elem: animdata->value){
                        anim->m_rotXvec.push_back(elem->value.toNumber());
                    }
                }
                else if(!strcmp(animdata->key, "rotYvec")){
                    for(auto elem: animdata->value){
                        anim->m_rotYvec.push_back(elem->value.toNumber());
                    }
                }
                else if(!strcmp(animdata->key, "offXvec")){
                    for(auto elem: animdata->value){
                        anim->m_offXvec.push_back(elem->value.toNumber());
                    }
                }
                else if(!strcmp(animdata->key, "offYvec")){
                    for(auto elem: animdata->value){
                        anim->m_offYvec.push_back(elem->value.toNumber());
                    }
                }
            }

            render->addAnimation(anim);
        }
    }

    return render;
}

Component* ComponentManager::createExitComponentFromJson(const JsonValue& value){
    ExitComponent* comp = new ExitComponent();

    for(auto data: value){
        if(!strcmp(data->key, "x"))
            comp->m_x = data->value.toNumber();
        if(!strcmp(data->key, "y"))
            comp->m_y = data->value.toNumber();
        if(!strcmp(data->key, "arena"))
            comp->m_arena = data->value.toNumber();
        if(!strcmp(data->key, "dir"))
            comp->m_direction = (direction)data->value.toNumber();
    }

    return comp;
}

Component* ComponentManager::createHealthComponentFromJson(const JsonValue& value){
    HealthComponent* comp = new HealthComponent();

    for(auto data: value){
        if(!strcmp(data->key, "max")){
            comp->m_max = data->value.toNumber();
            comp->m_current = data->value.toNumber();
        }
        if(!strcmp(data->key, "cur")){
            comp->m_current = data->value.toNumber();
        }
        if(!strcmp(data->key, "body")){
            comp->m_body = data->value.toString();
        }
    }

    return comp;
}

Component* ComponentManager::createMinimapComponentFromJson(const JsonValue& value){
    MinimapComponent* comp = new MinimapComponent();

    for(auto data: value){
        if(!strcmp(data->key, "symbol")){
            comp->m_symbol = Resources::sprites[data->value.toString()];
        }
    }

    return comp;
}
