#ifndef DISPLAYMANAGER_H_INCLUDED
#define DISPLAYMANAGER_H_INCLUDED

#include<allegro5/allegro.h>

class DisplayManager{
public:
    static DisplayManager& getInstance();
    const int screenPrefW = 850;
    const int screenPrefH = 480;

    ALLEGRO_DISPLAY* m_disp;

    int init(int flags);
    void deinit();
    void exit();

    void handleEvent(const ALLEGRO_EVENT& ev);
    void registerEventSource(ALLEGRO_EVENT_QUEUE* qu);

    void calculateTransformParameters();

    void calculateTransform();

    void useTransform();
    void useIdentity();

    template<typename T>
    void transformScreenToWindow(T* x, T* y){
        *x = (*x) / m_scaleX + m_offsetX;
        *y = (*y) / m_scaleY + m_offsetY;
    }

    template<typename T>
    void transformWindowToWorld(T* x, T* y){
        *x = (*x - m_offsetX) / m_scale;
        *y = (*y - m_offsetY) / m_scale;
    }

    template<typename T>
    void transformScreenToWorld(T* x, T* y){
        *x = (m_scaleX * (*x) / m_scale) + m_offsetX;
        *y = (m_scaleY * (*y) / m_scale) + m_offsetY;
    }
/*
    template<typename T>
    void transformWorldToScreen(T* x, T* y){
        *x = (*x - m_offsetX) * m_scale / m_scaleX + m_offsetX;
        *y = (*y - m_offsetY) * m_scale / m_scaleY + m_offsetY;
    }
*/
private:
    ALLEGRO_TRANSFORM m_trans;
    float m_scaleX;
    float m_scaleY;
    int m_scale;
    float m_offsetX;
    float m_offsetY;
};

#endif // DISPLAYMANAGER_H_INCLUDED
