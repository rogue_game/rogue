#pragma once

#include <map>
#include <vector>
#include "constants.h"
#include "types.h"

union JsonValue;
class Component;

#define PI 3.14159265

class Entity
{
    public:
        Entity();
        ~Entity();

        unsigned short int m_id;

private:
        compMap m_compMap;

    public:
        Component* getComponent(ComponentType compType);
        void setComponent(Component* component); //it won't check if the component is actually of the correct type :-(


        unsigned int get_id(){return m_id;}
};
