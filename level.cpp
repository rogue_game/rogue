#include "arena.h"
#include "component.h"
#include "displaymanager.h"
#include "entity.h"
#include "entitymanager.h"
#include "game.h"
#include "gason.h"
#include "level.h"
#include "tools.h"
#include "resources.h"

#include <string>
#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

Level& Level::getInstance()
{
    static Level instance;
    return instance;
}

Level::MapPos::MapPos(double x, double y, double w, double h)
: m_x(x), m_y(y), m_w(w), m_h(h)
{
}

Level::Level(){
    m_name = "Dummy";
    m_id = -1;

    m_minimap = al_create_bitmap(640, 480);
}

Level::~Level(){
    for(auto arena: m_arenas){
        delete arena;
    }
    m_arenas.clear();
    m_positions.clear();

    al_destroy_bitmap(m_minimap);
}

int Level::setActiveArena(int id)
{
    for(auto arena: m_arenas){
        if(arena->m_id == id){
            m_active = arena;
            drawArenaOnMinimap(id);
            return 0;
        }
    }

    return -1;
}

int Level::initFromFile(int id){
    m_id = id;
    std::string filepath = idToFilepath(id);

    unsigned int* buf = nullptr;
    unsigned int out_buf_size = 0;
    char* endptr = nullptr;

    if(!Utils::loadFile(filepath.c_str(), &buf, &out_buf_size))
    {
        fprintf(stderr, "loadFile: Error loading %s.\n", filepath.c_str());
        return -1;
    }

    JsonFile* jsonFile = new JsonFile();

    int status = jsonParse((char*)buf, &endptr, &jsonFile->value, jsonFile->alloc);
    if(status != JSON_OK)
    {
        fprintf(stderr, "jsonParse: Error loading %s: %s at %zd\n", filepath.c_str(), jsonStrError(status), endptr - (char*)buf);
        delete jsonFile;
        return -1;
    }

    for (auto i : jsonFile->value) {
        if(i->value.getTag() == JSON_STRING)
        {
            if(!strcmp(i->key, "name")){
                m_name = i->value.toString();
            }
        }
        else if(i->value.getTag() == JSON_OBJECT)
        {
            if(!strcmp(i->key, "arena"))
            {

                Arena* arena = new Arena();
                double mmx, mmy, mmw, mmh;
                for(auto j : i->value){
                    if(!strcmp(j->key, "id"))
                    {
                        arena->m_id = j->value.toNumber();
                        m_arenas.push_back(arena);

                        if(arena->initFromFile(j->value.toNumber()) == -1)
                        {
                            delete arena;
                            m_arenas.pop_back();
                            arena = NULL;
                            std::cout << "Failed to load arena " << j->key << "\n.";
                            return -1;
                        }
                    }
                    else if(!strcmp(j->key, "mmx")){
                        mmx = j->value.toNumber();
                    }
                    else if(!strcmp(j->key, "mmy")){
                        mmy = j->value.toNumber();
                    }
                    else if(!strcmp(j->key, "mmw")){
                        mmw = j->value.toNumber();
                    }
                    else if(!strcmp(j->key, "mmh")){
                        mmh = j->value.toNumber();
                    }
                }

                 if(arena != NULL)
                    m_positions.push_back(MapPos(mmx, mmy, mmw, mmh));
            }
        }
     }

    return 0;
}

std::string Level::idToFilepath(int levelID){
    std::string filepath = "data/levels/level";
    std::string strid = std::to_string(levelID);

    filepath.append(4 - strid.length(), '0');

    filepath += std::to_string(levelID)+".json";

    return filepath;
}

bool Level::placeEntity(Entity* ent, int arena_id)
{
    for(unsigned int i = 0; i < m_arenas.size(); i++){
        if(m_arenas[i]->m_id == arena_id){
            m_arenas[i]->placeEntity(ent);
            return true;
        }
    }

    return false;
}

bool Level::removeEntity(Entity* ent, int arena_id)
{
    for(unsigned int i = 0; i < m_arenas.size(); i++){
        if(m_arenas[i]->m_id == arena_id){
            m_arenas[i]->removeEntity(ent);
            return true;
        }
    }

    return false;
}

void Level::drawArenaOnMinimap(int arenaId)
{
    int w = al_get_bitmap_width(m_minimap);
    int h = al_get_bitmap_height(m_minimap);

    al_set_target_bitmap(m_minimap);

    al_draw_filled_rectangle(m_positions[arenaId].m_x*w, m_positions[arenaId].m_y*h,
                             (m_positions[arenaId].m_x+m_positions[arenaId].m_w)*w-1, (m_positions[arenaId].m_y+m_positions[arenaId].m_h)*h-1,
                             al_map_rgb(130,70,21));
    al_draw_rectangle(m_positions[arenaId].m_x*w, m_positions[arenaId].m_y*h,
                      (m_positions[arenaId].m_x+m_positions[arenaId].m_w)*w-1, (m_positions[arenaId].m_y+m_positions[arenaId].m_h)*h-1,
                      al_map_rgb(66,31,2), 1);

    for(auto entity: m_arenas[arenaId]->m_entities)
    {
        auto minimap = dynamic_cast<MinimapComponent*>(entity->getComponent(ComponentType::MINIMAP));
        auto pos = dynamic_cast<PositionComponent*>(entity->getComponent(ComponentType::POSITION));

        if(minimap != nullptr && pos != nullptr){
            al_draw_bitmap(minimap->m_symbol, (m_positions[arenaId].m_x+pos->m_x*m_positions[arenaId].m_w/DISPLAY_W)*w-3,
                           (m_positions[arenaId].m_y+pos->m_y*m_positions[arenaId].m_h/DISPLAY_H)*h-3,
                            0);
        }
    }

    al_set_target_bitmap(al_get_backbuffer(DisplayManager::getInstance().m_disp));
}

void Level::drawMinimap()
{

    drawArenaOnMinimap(m_active->m_id);

    int w = al_get_bitmap_width(m_minimap);
    int h = al_get_bitmap_height(m_minimap);

    //Use Camera position in the future rather than player position (now crashes when player Entity is deleted
    if(EntityManager::getInstance().m_player != nullptr){
        auto pos = dynamic_cast<PositionComponent*>(EntityManager::getInstance().m_player->getComponent(ComponentType::POSITION));

        m_minimap_cx = (m_positions[pos->m_arena_id].m_x + pos->m_x*m_positions[pos->m_arena_id].m_w/DISPLAY_W)*w;
        m_minimap_cy = (m_positions[pos->m_arena_id].m_y + pos->m_y*m_positions[pos->m_arena_id].m_h/DISPLAY_H)*h;
    }

    al_draw_bitmap_region(m_minimap, m_minimap_cx - 105, m_minimap_cy - 105, m_minimap_cx + 105, m_minimap_cy + 105, 640, 0, 0);
    al_draw_bitmap(Resources::sprites["minimap.bmp"], 640, 0, 0);

}
